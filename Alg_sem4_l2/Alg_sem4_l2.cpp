#include "pch.h"
#include "RB_Tree.cpp"
#include <string>
#include <iostream>
using namespace std;


int main()
{
	Map<string, double> tree;

	tree.insert("January", 15);
	tree.insert("February", 6);
	tree.insert("March", 9);
	tree.insert("April", 5);
	tree.insert("May", 8);
	tree.insert("June", 15);
	tree.insert("Jule", 1);
	tree.print(tree.root, 0);
	tree.remove("Jule"); //it is red node*/
	tree.remove("June");
	cout << *tree.get_keys() << endl;
	/*tree.insert("January", 15);
	tree.insert("February", 6);
	tree.insert("March", 9);
	tree.insert("April", 5);  // BUT HERE ALL RIGHT
	cout << "ROOT IS " << tree.root->key;*/
	tree.print(tree.root, 0);

	tree.clear();

	return 0;
}